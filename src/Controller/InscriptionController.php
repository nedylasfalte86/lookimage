<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\InscriptionType;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class InscriptionController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
}

    /**
     * @Route("/inscription", name="inscription")
     */
    public function index(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(InscriptionType::class, $user);

        $form->handleRequest($request);

        //si déja connecté redirige vers le compte de l'utilisateur
        if ($this->getUser()) {
            return $this->redirectToRoute('compte');
        }

            if ($form->isSubmitted() && $form->isValid()) {

                $user = $form->getData();

                //On stock l'encodagge des password dans la variable password
                $password = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
                //On envois les données en base
                //On apelle doctrine
                $doctrine = $this->getDoctrine()->getManager();
                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }

        return $this->render('inscription/index.html.twig', [
            'form' =>$form->createView()
        ]);
    }
}
