<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Picture;
use App\Entity\Categorie;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(paginatorInterface $paginator, Request $request): Response
    {

        //Equivalent d'un SELECT * FROM pictures
        //findAll()
        //retourne tous les résultats trouvés dans la table "pictures"
        $resultsPictures = $this->getDoctrine()->getRepository(Picture::class)->findAll();

        //Création pagination
        //Paginate() attends 3 arguments :
        //1. La collection contenant tous les résultats(pour nous, toutes les images)
        //2. La page sur laquelle nous sommes ( c'est à dire : page 1 etx..)
        //3. Le nombre d'éléments par page

        $page = $request->query->getInt('page', 1);
        $pictures  = $paginator->paginate(
            $resultsPictures,
            $page === 0 ? 1 : $page,
            12
        );

        //Permet de recuperer toutes les catégories
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        return $this->render('home/index.html.twig', [
            'pictures' => $pictures,
            'categories' => $categories
        ]);
    }

    //Création de la route vers la page catégorie désirée (lors du clique sur une catégorie)
    /**
     * @Route("/category/{id}", name="pictures_by_category")
     */
    public function picturesByCategory($id, paginatorInterface $paginator, Request $request)
    {
        //dd veut dire "Dump Data", équivalent de var_dump()
       // dd($id);

        //Ma méthode
      // $categories = $this->getDoctrine()->getRepository(Categorie::class)->findOneById($id);

        //find()  sélectionne un seul enregistrement selon son ID
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->find(['id' => $id]); //Méthode de Guillaume

        //Si la catégorie est introuvable, alors on génère une erreur 404
        if(!$categories){
            throw $this->createNotFoundException('La categorie n\'existe pas');
        }

        $page = $request->query->getInt('page', 1);
        $pictures  = $paginator->paginate(
            $categories->getPictures(),
            $page === 0 ? 1 : $page,
            12
        );

        return $this->render('home/picturesByCategory.html.twig', [
            'categories' => $categories,
            'pictures' => $pictures
        ]);
    }

    /**
     * "requirements" permet de valider le type de la donnée passée en paramètre
     * @Route("/show/picture/{id}", name="show_picture", requirements={"id"="\d+"})
     */
    public function showPicture($id, paginatorInterface $paginator, Request $request)
    {

        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);
        if (!$picture) {
            throw $this->createNotFoundException('Cette image n\'existe pas');
        }

        $page = $request->query->getInt('page', 1);
        $photos = $paginator->paginate(
            $picture->getCategorie()->getPictures(),
            $page === 0 ? 1 : $page,
            12
        );


        return $this->render('home/showPicture.html.twig', [
            'picture' => $picture,
            'photos' => $photos,
        ]);
    }
}
