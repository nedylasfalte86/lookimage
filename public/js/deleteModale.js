/**
 * deleteModal.js
 */
window.onload = function() {

    // Ecouteur d'évènement sur les boutons de suppression
    document.querySelectorAll('.confirm-delete').forEach(button => {
        button.addEventListener('click', function() {
           // let id = this.dataset.id;
           // let href = `/compte/delete/${id}`;

            let modal = document.querySelector('#supprimer');
            modal.querySelector('.btn-danger');
            button.href = this.dataset.href;

            var myModal = new bootstrap.Modal(modal);
            myModal.show();
        });
    });
}
